export class Content {
  id: any;
  memberId: any;
  service_type: any;
  __v: any;
  updated_at: any;
  created_at: any;
  isPrometed: any;
  offlineStatus: any;
  subscriptionType: any;
  downloadOptions: any;
  contentArray: any;
  mediaArray: any;
  viewOptions: any;
  tags: any;
  profanityCheck: any;
  content: any;
  mediaSrc: any;
  title: any;
  profilePicPath: any;
  profession: any;
  prefix: any;
  memberName: any;
}
