﻿export class Contracts {
  _id: any;
  memberId: any;
  serviceType: any;
  startDate: any;
  endDate: any;
  __v: any;
  updatedDateTime: any;
  updatedBy: any;
  createdDateTime: any;
  createdBy: any;
  remarks: any;
  isActive: any;
  serviceCredits: any;
}