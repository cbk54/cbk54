export class Orders{
  _id: any;
  cartId: any;
  financialTransId: any;
  serviceTransId: any;
  ordersStatus: any;
  __v: any;
  serviceStatus: any;
  updatedDateTime: any;
  updatedBy: any;
  createdDateTime: any;
  createdBy: any;
  orderServiceStatus: any;
  orderPaymentStatus: any;
  orderDateTime: any;
  orderType: any;
}
