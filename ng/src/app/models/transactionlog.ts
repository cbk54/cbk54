export class TransactionLogs{
  _id: any;
  memberId: any;
  financialType: any;
  __v: any;
  updated_at: any;
  created_at: any;
  paymentgatewayResponse: any;
  transactionRefNumber: any;
  transactionAuthCode: any;
  transactionValue: any;
  transactionMode: any;
  financialStatus: any;
}
