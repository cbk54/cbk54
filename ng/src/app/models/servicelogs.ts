export class Servicelogs{
  _id: any;
  service_type: any;
  senderId: any;
  receiverId: any;
  scheduleId: any;
  startTime: any;
  endTime: any;
  serviceStatus: any;
  __v: any;
  updatedAt: any;
  createdAt: any;
  serviceCode: any;
}
