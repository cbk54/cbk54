﻿export class Service {
  _id: any;
  service_type: any;
  senderId: any;
  receiverId: any;
  startTime: any;
  __v: any;
  endTime: any;
  updatedAt: any;
  createdAt: any;
  actualChargedCredits: any;
  transactionStatus: any;
  credits: any;
}