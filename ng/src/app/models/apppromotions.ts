export class apppromotions
{
  _id: any;
  memberId: any;
  __v: any;
  updatedAt: any;
  createdAt: any;
  rejectedReason: any;
  approvedDateTime: any;
  approvedBy: any;
  status: any;
  incentiveValue: any;
  reqDateTime: any;
}
