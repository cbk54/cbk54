export class Contents {
  id: any;
  memberId: any;
  title: any;
  service_type: any;
  __v: any;
  updated_at: any;
  created_at: any;
  isPrometed: any;
  offlineStatus: any;
  subscriptionType: any;
  //downloadOptions: any;
  //downloadStatus: any;
  contentArray: any;
  mediaArray: any;
  viewOptions: any;
  tags: any;
  profanityCheck: any;
  content: any;
  mediaSrc: any;
  profilePicPath: any;
  profession: any;
  prefix: any;
  memberName: any;
}
