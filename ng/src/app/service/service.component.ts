import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Service } from '../models/service';

import { AdminService } from '../services/admin.service';
@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit {

  showActions: any;
  serviceList: Service[];
  serviceId: any;

  constructor(public adminService: AdminService, public router: Router) {
  }

  showPopActions(id: any) {
    this.showActions = id;
    return id;
  }

  hidePopActions() {
    this.showActions = false;
  }

  ngOnInit() {
    this.onGetAllServices();
  }

  onGetAllServices() {
    this.adminService.onGetAllServices().subscribe(data => {
      this.serviceList = data;
      console.log(this.serviceList);
    });
  }

  onViewServiceById(id: any) {
    this.serviceId = id;
    this.router.navigate(['/viewservice/', { id: this.serviceId }]);
  }

}
