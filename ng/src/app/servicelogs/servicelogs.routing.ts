import { Routes } from '@angular/router';

import { ServicelogsComponent } from '../servicelogs/servicelogs.component';


export const ServicelogsRoutes: Routes = [
  {
    path: '',
    component: ServicelogsComponent,
    data: {
      heading: 'Service Logs'
    }
  }
];
