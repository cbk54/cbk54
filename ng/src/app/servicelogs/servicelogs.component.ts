import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Servicelogs } from '../models/servicelogs';

import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-servicelogs',
  templateUrl: './servicelogs.component.html',
  styleUrls: ['./servicelogs.component.scss']
})
export class ServicelogsComponent implements OnInit {

  showActions: any;
  servicelogsList: Servicelogs[];
  servicelogId: any;

  constructor(public adminService: AdminService, public router: Router) {
  }


  showPopActions(id: any) {
    this.showActions = id;
    return id;
  }

  hidePopActions() {
    this.showActions = false;
  }

  ngOnInit() {
    this.onGetAllServiceLogs();
  }

  onGetAllServiceLogs() {
    this.adminService.onGetAllServiceLogs().subscribe(data => {
      this.servicelogsList = data;
      console.log(this.servicelogsList);
    });
  }

  onViewServiceLogsById(id: any) {
    this.servicelogId = id;
    this.router.navigate(['/viewservicelog/', { id: this.servicelogId }]);
  }

}
