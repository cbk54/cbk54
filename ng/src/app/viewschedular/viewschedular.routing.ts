import { Routes } from '@angular/router';

import { ViewschedularComponent } from '../viewschedular/viewschedular.component';


export const ViewSchedularRoutes: Routes = [
  {
    path: '',
    component: ViewschedularComponent,
    data: {
      heading: 'View Activity'
    }
  }
];
