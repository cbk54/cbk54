import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AdminService } from '../services/admin.service';
import { Activities } from '../models/activities';

@Component({
  selector: 'app-viewschedular',
  templateUrl: './viewschedular.component.html',
  styleUrls: ['./viewschedular.component.scss']
})
export class ViewschedularComponent implements OnInit {

  activity: Activities;
  activityId: any;

  constructor(public adminService: AdminService, public router: Router, public route: ActivatedRoute) {
    this.activity = new Activities();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.activityId = params['id'];
      console.log(this.activityId);
    });

    this.onGetTransactionById();
  }

  onGetTransactionById() {

    this.adminService.onGetActivitiesByID(this.activityId)
      .subscribe(result => {
        this.activity = result;
        console.log("activity:" + this.activity);

      });
    return this.activity;
  }

}
