import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewschedularComponent } from './viewschedular.component';

describe('ViewschedularComponent', () => {
  let component: ViewschedularComponent;
  let fixture: ComponentFixture<ViewschedularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewschedularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewschedularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
