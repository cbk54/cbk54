import { Routes } from '@angular/router';

import { VieworderComponent } from '../vieworder/vieworder.component';

export const ViewOrderRoutes: Routes = [{
  //path: 'ViewProfile/:id',
  path:'',
  component: VieworderComponent,
  data: {
    heading: 'vieworder'
  }
 //{ path: '',
 //   component: ViewprofileComponent,
 //   children: [
 //     {
 //       path: 'ViewProfile/:id',
 //       component: ViewprofileComponent,
 //       canDeactivate: [CanDeactivateGuard],
 //       resolve: {
 //         crisis: CrisisDetailResolver
 //       }
 //     }
}];
