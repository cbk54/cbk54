import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AdminService } from '../services/admin.service';
import { Orders } from '../models/orders';

@Component({
  selector: 'app-vieworder',
  templateUrl: './vieworder.component.html',
  styleUrls: ['./vieworder.component.scss']
})
export class VieworderComponent implements OnInit {

  order: Orders;
  orderId: any;

  constructor(public adminService: AdminService, public router: Router, public route: ActivatedRoute) {
    this.order = new Orders();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.orderId = params['id'];
      console.log(this.orderId);
    });

    this.onGetOrderById();
  }

  onGetOrderById() {

    this.adminService.onGetOrdersByID(this.orderId)
      .subscribe(result => {
        this.order = result;
        console.log("order:" + this.order);

      });
    return this.order;
  }

}
