import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';

import { EventLogs } from '../models/eventlogs';

@Component({
  selector: 'app-eventlog',
  templateUrl: './eventlog.component.html',
  styleUrls: ['./eventlog.component.scss']
})
export class EventlogComponent implements OnInit {

  showActions: any;
  eventLogsList: EventLogs[];
  eventLogId: any;

  constructor(public adminService: AdminService, public router: Router) {
  }


  showPopActions(id: any) {
    this.showActions = id;
    return id;
  }

  hidePopActions() {
    this.showActions = false;
  }

  ngOnInit() {
    this.onGetAllEventLogs();
  }

  onGetAllEventLogs() {
    this.adminService.onGetAllEventLogs().subscribe(data => {
      this.eventLogsList = data;
      console.log(this.eventLogsList);
    });
  }

  //onViewEventLogById(id: any) {
  //  this.eventLogId = id;
  //  this.router.navigate(['/vieweventlog/', { id: this.eventLogId }]);
  //}

}
