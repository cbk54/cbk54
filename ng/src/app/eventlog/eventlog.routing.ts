import { Routes } from '@angular/router';

import { EventlogComponent } from '../eventlog/eventlog.component';


export const EventlogRoutes: Routes = [
  {
    path: '',
    component: EventlogComponent,
    data: {
      heading: 'Event Logs'
    }
  }
];
