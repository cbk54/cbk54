import { Routes } from '@angular/router';

import { ApppromotionsComponent } from '../apppromotions/apppromotions.component';


export const AppPromotionsRoutes: Routes = [
  {
    path: '',
    component: ApppromotionsComponent,
    data: {
      heading: 'AppPromotions'
    }
  }
];
