import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AdminService } from '../services/admin.service';
import { apppromotions } from '../models/apppromotions';

@Component({
  selector: 'app-apppromotions',
  templateUrl: './apppromotions.component.html',
  styleUrls: ['./apppromotions.component.scss']
})
export class ApppromotionsComponent implements OnInit {
  private showActions: any;
  constructor(private adminService: AdminService, public router: Router) {
  }

  private promotions: any[];

  ngOnInit() {
    this.ongetallAppPromotions();
  }

  showPopActions(id) {
   // alert(id);
    this.router.navigate(['/viewpromotions/', {id:id}]);    
  }


  hidePopActions() {
    this.showActions = false;
  }

  ongetallAppPromotions() {
    this.adminService.onGetallAppPromotions().subscribe(data => {
      this.promotions = data;
      console.log(this.promotions);
    });

  }

  ongetapppromotionsById(id: any) {
    this.router.navigate(['/ViewPromotions/', { id: id }]);

  }


  //onviewcelebbyid(memId: any) {
  //  this.celebid = memId;
  //  //this.adminService.onGetMemberById(this.celebid);
  //  this.router.navigate(['/ViewProfile/', { memId: this.celebid }]);
  //}

}
