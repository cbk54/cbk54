import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApppromotionsComponent } from './apppromotions.component';

describe('ApppromotionsComponent', () => {
  let component: ApppromotionsComponent;
  let fixture: ComponentFixture<ApppromotionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApppromotionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApppromotionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
