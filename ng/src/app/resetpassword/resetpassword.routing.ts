import { Routes } from '@angular/router';

import { ResetpasswordComponent } from '../resetpassword/resetpassword.component';


export const ResetPasswordRoutes: Routes = [
  {
    path: '',
    component: ResetpasswordComponent,
    data: {
      heading: 'ResetPassword'
    }
  }
];
