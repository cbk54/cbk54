import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import { AdminService } from '../services/admin.service';

import { Celebs } from '../models/celebs';

const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  public form: FormGroup;
  hello: any;
  memberId: any;
  celebs: Celebs;
  password: any;
  confirmPassword: any;
  constructor(private fb: FormBuilder,private adminService: AdminService, public router: Router, public route: ActivatedRoute)
  {
  
  }
  onResetFormGroup() {
    this.form = this.fb.group({
      password: password,
      confirmPassword: confirmPassword
    });
  }

  ngOnInit() {
    this.onResetFormGroup();    
   // this.onresetpassword(this.memberId);
    this.route.params.subscribe(params => {
      this.memberId = params['id'];
    });
    
  }


 


  onReset(id: any, password: any) {
    alert(this.memberId);
    alert(id);
    password =password;
    alert(password);
    this.adminService.onReset(id, password).subscribe(data => {
      console.log(data);
    });
  }
  //ongetmemberbyid() {
  //  this.adminService.onGetMemberById(this.memberId)
  //    .subscribe(result => {
  //      this.celebs = result;
  //      this.hello = "Say Hi";
  //      return result;
  //      // this.router.navigate(['/', 'viewprofile', this.celebid]);
  //     // console.log(result);
  //    });
  //}


  //onresetpassword(id) {
  //  id = this.memberId;
  //  alert(id);
  //  return id;
  //}



}
