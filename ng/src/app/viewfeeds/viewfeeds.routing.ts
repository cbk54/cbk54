import { Routes } from '@angular/router';

import { ViewfeedsComponent } from '../viewfeeds/viewfeeds.component';

export const ViewfeedsRoutes: Routes = [{
  //path: 'ViewProfile/:id',
  path:'',
  component: ViewfeedsComponent,
  data: {
    heading: 'viewfeeds'
  }
 //{ path: '',
 //   component: ViewprofileComponent,
 //   children: [
 //     {
 //       path: 'ViewProfile/:id',
 //       component: ViewprofileComponent,
 //       canDeactivate: [CanDeactivateGuard],
 //       resolve: {
 //         crisis: CrisisDetailResolver
 //       }
 //     }
}];
