import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//import { Pipe, PipeTransform } from 'angular2/core';
import { AdminService } from '../services/admin.service';

import { Contents } from '../models/members';

@Component({
  selector: 'app-viewfeeds',
  templateUrl: './viewfeeds.component.html',
  styleUrls: ['./viewfeeds.component.scss']
})
export class ViewfeedsComponent implements OnInit {

  feedId: any;
  feedList: Contents;
  //contents: Array<any> = [];
  imageUrl: any = "http://13.58.150.195:4300";
  imageUrlWithSlash: any = "http://13.58.150.195:4300/";
  keys: any;
  constructor(public router: Router, public route: ActivatedRoute, public adminService: AdminService) {
    this.feedList = new Contents();
    //this.contents = new Array<any>();
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.feedId = params['feedId'];
      console.log(this.feedId);
    });

    this.ongetfeedbyid();
  }

  ongetfeedbyid() {
    this.adminService.onGetFeedById(this.feedId)
      .subscribe(result => {
        this.feedList = result;
        //this.feedList.mediaSrc = this.feedList.mediaSrc.replace("/", " ");
        //this.contents = this.feedList.contentArray;
        //this.keys = Object.keys(this.feedList);
        console.log(this.feedList);    
      });
    return this.feedList;
  }

  //onchecked() {
  //  alert("hi");
  //}

  onUpdateSettings(settings) {
   // alert(settings.downloadOptions.downloadStatus);
    //alert(settings.offlineStatus);
    //alert(settings.profanityCheck);
   // alert(settings.viewOptions);
    this.adminService.onUpdateContentSettings(settings).subscribe(result => {
      console.log(result);
    });
  }


  OnBoostSettingsUpdate(settings) {
    //alert(settings._id);
    //alert(settings.boostSettings.proposedBudget);
    //alert(settings.boostSettings.proposedReach);
    this.adminService.onUpdateBootSettings(settings).subscribe(result => {
      console.log(result);
    });

  }

}
