import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewfeedsComponent } from './viewfeeds.component';

describe('ViewfeedsComponent', () => {
  let component: ViewfeedsComponent;
  let fixture: ComponentFixture<ViewfeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewfeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewfeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
