import { Routes } from '@angular/router';

import { SigninComponent } from './authentication/signin/signin.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
//import { ViewprofileComponent } from './viewprofile/viewprofile.component';


export const AppRoutes: Routes = [
  {
    path: '',
    component: SigninComponent,
    children: [{
      path: 'signin',
      loadChildren: './authentication/authentication.module#AuthenticationModule'
    }],
  },
  {
  path: '',
  component: AdminLayoutComponent,
  children: [{
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  }, {
      path: 'celebrities',
      loadChildren: './celebrities/celebrities.module#CelebritiesModule'
    }, {
      path: 'members',
      loadChildren: './members/members.module#MembersModule'
  },
    {
      path: 'contentmanagement',
      loadChildren: './contentmanagement/contentmanagement.module#ContentmanagementModule'
    },
    {
      path: 'inventorymanagement',
      loadChildren: './inventorymanagement/inventorymanagement.module#InventorymanagementModule'
    },
    {
      path: 'orders',
      loadChildren: './orders/orders.module#OrdersModule'
    },
    {
      path: 'scheduler',
      loadChildren: './scheduler/scheduler.module#SchedulerModule'
    }, {
      path: 'eventlog',
      loadChildren: './eventlog/eventlog.module#EventlogModule'
    },
    {
      path: 'transactionlog',
      loadChildren: './transactionlog/transactionlog.module#TransactionlogModule'
    },
    {
      path: 'usermanagement',
      loadChildren: './usermanagement/usermanagement.module#UsermanagementModule'
    },
    {
      path: 'auctions',
      loadChildren: './auctions/auctions.module#AuctionsModule'
    },
    {
    path: 'email',
    loadChildren: './email/email.module#EmailModule'
  }, {
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule'
  }, {
    path: 'icons',
    loadChildren: './icons/icons.module#IconsModule'
  }, {
    path: 'cards',
    loadChildren: './cards/cards.module#CardsModule'
  }, {
    path: 'forms',
    loadChildren: './form/form.module#FormModule'
  }, {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule'
  }, {
    path: 'datatable',
    loadChildren: './datatable/datatable.module#DatatableModule'
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule'
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule'
  }, {
    path: 'pages',
    loadChildren: './pages/pages.module#PagesModule'
  }, {
    path: 'taskboard',
    loadChildren: './taskboard/taskboard.module#TaskboardModule'
  }, {
    path: 'calendar',
    loadChildren: './fullcalendar/fullcalendar.module#FullcalendarModule'
  }, {
    path: 'media',
    loadChildren: './media/media.module#MediaModule'
  }, {
    path: 'widgets',
    loadChildren: './widgets/widgets.module#WidgetsModule'
  }, {
    path: 'social',
    loadChildren: './social/social.module#SocialModule'
  }, {
    path: 'docs',
    loadChildren: './docs/docs.module#DocsModule'
  }, {
    path: 'ViewProfile',
    loadChildren: './viewprofile/viewprofile.module#ViewProfileModule'
  },
   {
    path: 'resetpassword',
    loadChildren:'./resetpassword/resetpassword.module#ResetPasswordModule'
  },
  {
    path: 'viewfeeds',
    loadChildren: './viewfeeds/viewfeeds.module#ViewfeedsModule'
   },
   {
     path: 'vieworder',
     loadChildren: './vieworder/vieworder.module#ViewOrderModule'
  },
  {
    path: 'viewtransaction',
    loadChildren: './viewtransaction/viewtransaction.module#ViewTransactionModule'
   },
   {
     path: 'viewschedular',
     loadChildren: './viewschedular/viewschedular.module#ViewSchedularModule'
  },
  {
    path: 'servicelogs',
    loadChildren: './servicelogs/servicelogs.module#ServicelogsModule'
   },
   {
     path: 'service',
     loadChildren: './service/service.module#ServiceModule'
  },
  {
    path: 'viewservicelog',
    loadChildren: './viewservicelog/viewservicelog.module#ViewServiceLogsModule'
   },
   {
     path: 'viewservice',
     loadChildren: './viewservice/viewservice.module#ViewserviceModule'
  }, {
    path: 'apppromotions',
    loadChildren: './apppromotions/apppromotions.module#ApppromotionsModule'
  },
  {
    path: 'viewpromotions',
    loadChildren: './viewpromotions/viewpromotions.module#ViewPromotionsModule'
  },
  {
    path: 'celebrequests',
    loadChildren: './celebrequests/celebrequests.module#CelebrequestsModule'
  },
  //{
  //  path: 'viewcelebrequests',
  //  loadChildren: './viewcelebrequests/viewcelebrequests.module#ViewCelebRequestsModule'
  //},


    //, {
  //    path: 'ViewProfile/:id',
  //    //redirectTo: 'viewprofile',
  //  loadChildren: './viewprofile/viewprofile.module#ViewProfileModule'
  //}
  ]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [{
    path: 'authentication',
    loadChildren: './authentication/authentication.module#AuthenticationModule'
  }, {
    path: 'error',
    loadChildren: './error/error.module#ErrorModule'
  }, {
    path: 'landing',
    loadChildren: './landing/landing.module#LandingModule'
  }]
}, {
  path: '**',
  redirectTo: 'error/404'
}
];

