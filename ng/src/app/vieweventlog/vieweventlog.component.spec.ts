import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VieweventlogComponent } from './vieweventlog.component';

describe('VieweventlogComponent', () => {
  let component: VieweventlogComponent;
  let fixture: ComponentFixture<VieweventlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VieweventlogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VieweventlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
