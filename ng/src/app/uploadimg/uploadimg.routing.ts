import { Routes } from '@angular/router';

import { UploadimgComponent } from '../uploadimg/uploadimg.component';


export const UploadimgRoutes: Routes = [
  {
    path: '',
    component: UploadimgComponent,
    data: {
      heading: 'uploadimg'
    }
  }
];
