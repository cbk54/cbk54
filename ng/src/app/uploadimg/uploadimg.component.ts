import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { FileUploader } from 'ng2-file-upload';

import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-uploadimg',
  templateUrl: './uploadimg.component.html',
  styleUrls: ['./uploadimg.component.scss']
})
export class UploadimgComponent implements OnInit {
  http: any;

  form: FormGroup;
  loading: boolean = false;

  public uploader: FileUploader = new FileUploader({ url: 'http://13.58.150.195:4300/users/editUser' });

   
  @ViewChild('fileInput') fileInput: ElementRef;


  constructor(private fb: FormBuilder, public adminService: AdminService, private ele: ElementRef) {
    this.createForm();}

  ngOnInit() {
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      avatar: null
    });
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.form.get('avatar').setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        })
      };
    }
  }

  onSubmit() {
    const formModel = this.form.value;
    alert(JSON.stringify(formModel));
    this.loading = true;
    let id: "";
    this.adminService.onUploadProfilePic(id,formModel);
    // In a real-world app you'd have a http request / service call here like
   
   // this.http.post('http://13.58.150.195:4300/users/editUser', formModel)
    setTimeout(() => {
      console.log(formModel);
      alert('done!');
      this.loading = false;
    }, 1000);
  }

  clearFile() {
    this.form.get('avatar').setValue(null);
    this.fileInput.nativeElement.value = '';
  }

  uploadImage() {
   // alert('hi');
    this.ele.nativeElement.querySelector('#spinner')
    let id = "4654546465465";
    let files = this.ele.nativeElement.querySelector('#selectFile').files;
    let req = files;
    let formData = new FormData();
    let file = files[0];
    formData.append('selectFile', file, file.name);
    formData.append('avtars', file, file.id);
    console.log(formData);
    console.log(files);
    this.adminService.onfileupload(id,formData).subscribe(res => this.dataLoaded(res));   

  }

  private dataLoaded(date: any) {
    this.ele.nativeElement.querySelector('#spinner').style.visibility = 'hidden';
  }


}



