import { Routes } from '@angular/router';

import { ViewtransactionComponent } from '../viewtransaction/viewtransaction.component';

export const ViewTransactionRoutes: Routes = [{
  //path: 'ViewProfile/:id',
  path:'',
  component: ViewtransactionComponent,
  data: {
    heading: 'View Transaction'
  }
 //{ path: '',
 //   component: ViewprofileComponent,
 //   children: [
 //     {
 //       path: 'ViewProfile/:id',
 //       component: ViewprofileComponent,
 //       canDeactivate: [CanDeactivateGuard],
 //       resolve: {
 //         crisis: CrisisDetailResolver
 //       }
 //     }
}];
