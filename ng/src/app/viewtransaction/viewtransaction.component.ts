import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AdminService } from '../services/admin.service';
import { TransactionLogs } from '../models/transactionlog';


@Component({
  selector: 'app-viewtransaction',
  templateUrl: './viewtransaction.component.html',
  styleUrls: ['./viewtransaction.component.scss']
})
export class ViewtransactionComponent implements OnInit {

  transaction: TransactionLogs;
  transactionId: any;

  constructor(public adminService: AdminService, public router: Router, public route: ActivatedRoute) {
    this.transaction = new TransactionLogs();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.transactionId = params['id'];
      console.log(this.transactionId);
    });

    this.onGetTransactionById();
  }

  onGetTransactionById() {
    
    this.adminService.onGetTransactionLogByID(this.transactionId)
      .subscribe(result => {
        this.transaction = result;
        console.log("transaction:" + this.transaction);
          
      });
    return this.transaction;
  }

}
