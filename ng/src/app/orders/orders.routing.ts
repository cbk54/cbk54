import { Routes } from '@angular/router';

import { OrdersComponent } from '../orders/orders.component';


export const OrdersRoutes: Routes = [
  {
    path: '',
    component: OrdersComponent,
    data: {
      heading: 'Orders'
    }
  }
];
