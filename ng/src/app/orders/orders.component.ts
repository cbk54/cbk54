import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';

import { Orders } from '../models/orders';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  showActions: any;
  ordersList: Orders[];
  orderId: any;

  constructor(public adminService: AdminService, public router: Router) {
  }


  showPopActions(id: any) {
    this.showActions = id;
    return id;
  }

  hidePopActions() {
    this.showActions = false;
  }

  ngOnInit() {
    this.onGetAllOrders();
  }

  onGetAllOrders() {
    this.adminService.onGetAllOrders().subscribe(data => {
      this.ordersList = data;
      console.log(this.ordersList);
    });
  }

  onViewOrdersById(id: any) {
    this.orderId = id;
    this.router.navigate(['/vieworder/', { id: this.orderId }]);
  }

  OnOrderStatusChange(value, item) {
    alert(value);
    alert(item._id);
    this.adminService.onUpdateOrderStatus(value, item).subscribe(data => { console.log("Updated Result " + data) });
  }

}
