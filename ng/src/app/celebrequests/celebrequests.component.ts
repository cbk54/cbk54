import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-celebrequests',
  templateUrl: './celebrequests.component.html',
  styleUrls: ['./celebrequests.component.scss']
})
export class CelebrequestsComponent implements OnInit {
  celebrequests: any[];

  constructor(public adminservice: AdminService, public router : Router) { }

  ngOnInit() {
    this.onGetAllCelebrequests();
  }

  onGetAllCelebrequests() {
    this.adminservice.onGetAllCelebRequests().subscribe(
      data => {
      this.celebrequests = data;
      console.log(this.celebrequests);
      });
    return this.celebrequests;
  }

  showPopActions(id) {
   //  alert(id);
   // this.router.navigate(['/viewcelebrequests/', { id: id }]);
    // this.router.navigate(['/viewprofile/', { memId: id }]);
     this.router.navigate(['/ViewProfile/', { memId: id }]);
  }
 

}
