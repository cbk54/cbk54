import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CelebrequestsComponent } from './celebrequests.component';

describe('CelebrequestsComponent', () => {
  let component: CelebrequestsComponent;
  let fixture: ComponentFixture<CelebrequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CelebrequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CelebrequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
