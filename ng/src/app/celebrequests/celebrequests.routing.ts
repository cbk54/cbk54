import { Routes } from '@angular/router';

import { CelebrequestsComponent } from '../celebrequests/celebrequests.component';


export const CelebrequestsRoutes: Routes = [
  {
    path: '',
    component: CelebrequestsComponent,
    data: {
      heading: 'Celebrity Requests'
    }
  }
];
