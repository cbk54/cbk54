import { Routes } from '@angular/router';

import { ViewprofileComponent } from './viewprofile.component';

export const ViewProfileRoutes: Routes = [{
  //path: 'ViewProfile/:id',
  path:'',
  component: ViewprofileComponent,
  data: {
    heading: 'View Profile'
  }
 //{ path: '',
 //   component: ViewprofileComponent,
 //   children: [
 //     {
 //       path: 'ViewProfile/:id',
 //       component: ViewprofileComponent,
 //       canDeactivate: [CanDeactivateGuard],
 //       resolve: {
 //         crisis: CrisisDetailResolver
 //       }
 //     }
}];
