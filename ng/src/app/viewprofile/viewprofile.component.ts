import { Component, OnInit, Input, ElementRef, ViewChild} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/share';

//import { Component, ElementRef, ViewChild } from '@angular/core';

import { AdminService } from '../services/admin.service';
import { Celebs } from '../models/celebs';
import { Observable } from 'rxjs/Observable';
import { Content } from '../models/content';
import { TransactionLogs } from '../models/transactionlog';
import { Contracts } from '../models/contracts';


//declare var snackBarTimeout: any;

export interface IAlert {
  id: number;
  type: string;
  message: string;
}


const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));
//const profile = new FormControl('');
//const  imageUrl:any = "http://13.58.150.195:4300/avtars/" ;

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.scss'],
  providers: [DatePipe]
})




export class ViewprofileComponent implements OnInit {

  public form: FormGroup;
  public memberUpdateFormGroup: FormGroup;

  //public formProfile: FormGroup;
  closeResult: string;
  loading: boolean = false;

  @ViewChild('fileInput') fileInput: ElementRef;

  @Input()
  public alerts: Array<IAlert> = [];
  celebs: Celebs;
  memberId: any;
  name: any;
  images: any;
  num = 1;
  private showActionsOne: boolean;
  private showActionsTwo: boolean;
  isSuccess: boolean;
  isSuccessPwd: boolean;
  isError: boolean;
  successMessage: string;
  errorValidaionMessage: string;
  successMessagePwd: string;
  date: any;
  startDate: any;
  endDate: any;
  private _success = new Subject<string>();
  staticAlertClosed = false;
  imageUrl: any = "http://13.58.150.195:4300/";
  imageUrlPath: any = "http://13.58.150.195:4300";
  oldImage: any;
  newImage: any;
  filesToUpload: any;
  address: any;
  content2: Content[];
  errorMessage: string;
  dataLoaded: any;

  showActions: any;
  transactionLogList: TransactionLogs[];
  transactionId: any;

  contractsList: Contracts[];
  contracts: Contracts;
  successMessageContract: string;
  isSuccessContract: boolean;

  //http: any;
  //addNewFormGroup: FormGroup;
//  form: FormGroup;
 

  constructor(private ele: ElementRef,private http: Http,private fb: FormBuilder, public adminService: AdminService, public router: Router, public route: ActivatedRoute, private modalService: NgbModal, public datepipe: DatePipe ) {
    this.celebs = new Celebs();
    this.contracts = new Contracts();
  
    this.showActionsOne = false;
    this.showActionsTwo = false;
    this.isSuccess = false;
    this.isError = false;
    this.isSuccessPwd = false;
    this.isSuccessContract = false;
    //this.createAddNewForm();
    //for (this.num; this.num <= 18; this.num += 1) {
    //  this.images.push(this.num);
    //}

    //this.createForm();
  }

  showPopActions(id: any) {
    this.showActions = id;
    return id;
  }

  hidePopActions() {
    this.showActions = false;
  }

  //createForm() {
  //  this.form = this.fbb.group({
  //    name: ['', Validators.required],
  //    avatar: null
  //  });
  //}

  onAlertType() {
    this.alerts.push({
      id: 1,
      type: 'success',
      message: 'This is an success alert',
    }, {
        id: 2,
        type: 'info',
        message: 'This is an info alert',
      }, {
        id: 3,
        type: 'warning',
        message: 'This is a warning alert',
      }, {
        id: 4,
        type: 'danger',
        message: 'This is a danger alert',
      });
  }

  onResetFormGroup() {
    this.form = this.fb.group({
      password: password,
      confirmPassword: confirmPassword
    });
  }

  onUpdateMemberForm(){
    this.memberUpdateFormGroup = this.fb.group({
      'celebs.name': ['', [Validators.required]],
      'celebs.lastName': ['', [Validators.required]],
      'celebs.mobileNumber': ['', [Validators.required]],
      'celebs.email': ['', [Validators.required]],
      'celebs.location': ['', [Validators.required]],
      'celebs.aboutMe': ['', [Validators.required]],
      'celebs.gender': ['', [Validators.required]],
      'celebs.dateOfBirth': ['', [Validators.required]],
      'celebs.isCeleb': ['', [Validators.required]],
      'celebs.status': ['', [Validators.required]],
      'celebs.isPromoted': ['',[Validators.required]],
      'celebs.isEditorChoice': ['', [Validators.required]],
      'celebs.isOnline': ['',[Validators.required]],
      'celebs.isTrending': ['', [Validators.required]],
    });
  }

  //onUploadProfilePicFormGroup() {
  //  this.formProfile = this.fb.group({
  //    image: profile,
  //  });
  //}


  ngOnInit() {

    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.successMessage = message);
    this._success.debounceTime(5000).subscribe(() => this.successMessage = null);

    this._success.subscribe((message) => this.successMessagePwd = message);
    this._success.debounceTime(5000).subscribe(() => this.successMessagePwd = null);

    this._success.subscribe((message) => this.successMessageContract = message);
    this._success.debounceTime(5000).subscribe(() => this.successMessageContract = null);

    this._success.subscribe((message) => this.errorValidaionMessage = message);
    this._success.debounceTime(5000).subscribe(() => this.errorValidaionMessage = null);

    this.onResetFormGroup();
    this.onUpdateMemberForm();

    this.route.params.subscribe(params => {
      this.memberId = params['memId'];
      console.log(this.memberId);
    });
    this.ongetmemberbyid();
    this.onGetContentByID();
    this.onGetTransactionLogByUserID();
    this.onGetAllContractsByMemberId();
  }

  ongetmemberbyid() {
    
    this.date = new Date();
   this.adminService.onGetMemberById(this.memberId)
      .subscribe(result => {
        this.celebs = result;
        console.log("celebs:"+ this.celebs);
        if (this.celebs.dateOfBirth == null && this.celebs.dateOfBirth == "" && this.celebs.dateOfBirth == undefined) {
          this.date = this.datepipe.transform(this.celebs.dateOfBirth, 'yyyy-MM-dd');
          this.celebs.dateOfBirth = this.date;
        }
        this.oldImage = this.celebs.avtar_imgPath;
        //let re = /\-/gi;
        //this.celebs.dateOfBirth=this.celebs.dateOfBirth.replace(re, "/");

        console.log(this.celebs);
             
      });
   return this.celebs;
  }

  onGetAllContractsByMemberId() {
    this.adminService.onGetAllContractsByMemberId(this.memberId).subscribe(data => {
      this.contractsList = data;
      console.log(this.contractsList);
    });
  }

  onGetAllContractsByContractId(id: any) {
    this.startDate = new Date();
    this.endDate = new Date();
    this.adminService.onGetAllContractsByContractId(id).subscribe(data => {
      this.contracts = data;
      console.log(this.contracts);
      if (this.contracts.startDate != null && this.contracts.startDate != "" && this.contracts.startDate != undefined) {
        this.startDate = this.datepipe.transform(this.contracts.startDate, 'yyyy-MM-dd');
        this.contracts.startDate = this.startDate;
      }

      if (this.contracts.endDate != null && this.contracts.endDate != "" && this.contracts.endDate != undefined) {
        this.endDate = this.datepipe.transform(this.contracts.endDate, 'yyyy-MM-dd');
        this.contracts.endDate = this.endDate;
        console.log(this.contracts.endDate);
      }
    });
  }

  //onconvert(str) {
  //var mnths = {
  //  Jan: "01", Feb: "02", Mar: "03", Apr: "04", May: "05", Jun: "06",
  //  Jul: "07", Aug: "08", Sep: "09", Oct: "10", Nov: "11", Dec: "12"
  //},
  //    date = str.split(" ");
  //console.log(date);

  //return [date[3], mnths[date[1]], date[2]].join("-");
  //}

  //onFileChange(event) {
  //  if (event.target.files.length > 0) {
  //    let file = event.target.files[0];
  //    //this.form.get('file-input').setValue(file);
  //  }
  //}

  //private prepareSave(): any {
  //  let input = new FormData();
  //  input.append('name', this.form.get('name').value);
  //  input.append('file-input', this.form.get('file-input').value);
  //  return input;
  //}

  //onSubmit2() {
  //  const formModel = this.prepareSave();
  //  this.loading = true;
  //  // In a real-world app you'd have a http request / service call here like
  //  this.http.post('http://13.58.150.195:4300/users/editUser', formModel)
  //  setTimeout(() => {
  //    // FormData cannot be inspected (see "Key difference"), hence no need to log it here
  //    alert('done!');
  //    this.loading = false;
  //  }, 1000);
  //}

  //clearFile() {
  //  this.form.get('file-input').setValue(null);
  //  this.fileInput.nativeElement.value = '';
  //}

  //onFileChange(event) {
  //  let reader = new FileReader();
  //  if (event.target.files && event.target.files.length > 0) {
  //    let file = event.target.files[0];
  //    reader.readAsDataURL(file);
  //    reader.onload = () => {
  //      this.form.get('file-input').setValue({
  //        filename: file.name,
  //        filetype: file.type,
  //        value: reader.result.split(',')[1]
  //      })
  //    };
  //  }

  //  const formModel = this.form.value;
  //  this.loading = true;
  //  // In a real-world app you'd have a http request / service call here like
  ////  this.http.post('http://13.58.150.195:4300/users/editUser', formModel)
  //  setTimeout(() => {
  //    console.log(formModel);
  //    alert('done!');
  //    this.loading = false;
  //  }, 1000);

  //}



  readURL(event: any) {
    ////let input = new FormData();
    ////input.append('file-input', this.formProfile.get('file-input').value);
    ////console.log("input" + input);
    ////return input;

    let fileList =  event.target.files || event.srcElement.files;

    this.images = fileList[0].name;
    this.filesToUpload = fileList;

    let formd: FormData = new FormData();
     
    ////formd.append('image', this.filesToUpload, this.filesToUpload.name);
    ////for (let i = 0; i < fileList.length; i++) {
    formd.append("avtars", fileList[0], fileList[0].name);
    //var options = { content: formd };
    ////}
    ////formd.append('avtars', this.filesToUpload, this.filesToUpload.name);
    console.log('form data variable :   ' + formd);

    //let fileList: FileList = event.target.files;
    //this.images = fileList[0].name;

    ////this.http.post('http://13.58.150.195:4300/users/editUser', formd).map(files => files.json()).subscribe(files => console.log('files',files))

 //const files = event.target.files || event.srcElement.files;
    ////const file = files[0];
    ////const formData = new FormData();
    ////formData.append('file', file);
    
    ////const formData: any = new FormData();
    ////const files: Array<File> = this.filesToUpload;

    //////formData.append("files", fileList[0], fileList[0]['name']);
    //console.log(formData);

    //this.adminService.onUploadProfilePic(this.memberId,formData)
    //  .subscribe(result => {
    //    console.log(result);
    //});

    //this.http.post('http://13.58.150.195:4300/users/editUser', formd)
    //  .map(files => files.json())
    //  .subscribe(files => console.log('files', files))
  

    //let files = event.target.files;
   
    //let formData: FormData = new FormData();
    //for (let file of files) {
    //  formData.append('avtars', file, file.name);
    //}
    //let headers = new Headers();
    //headers.set('Accept', 'application/json');
    ////  let options = new RequestOptions({ headers: headers });
    //this.adminService.onUploadProfilePic(this.memberId,formData)
    //  // this.http.post("http://13.58.150.195:4300/users/editUser", formData)
    //  .subscribe(
    //  data => {
    //    // Consume Files
    //    // ..
    //    console.log(data);
    //  },
    //  error => console.log(error),
    //  () => {
    //    console.log('an error occurred');
    //  });
  }

  onUpdateProfile(member: any) {

    let files = this.ele.nativeElement.querySelector('#selectFile').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let req = files;
      let formData = new FormData();
      let file = files[0];
      this.images = file.name;

      formData.append('selectFile', file, file.name);
      formData.append('avtars', file, file.id);
      console.log(formData);
      console.log(files);
      this.adminService.onfileupload(this.memberId, formData).subscribe(res => this.dataLoaded=res);
    }
    if (this.images != null && this.images != "") {
      member.avtar_originalname = this.images.toString();

      member.avtar_imgPath = "avtars/" + this.images.toString();
    }
    else {
      member.avtar_imgPath = this.oldImage;
    }
    //member.password = "$2a$10$sI.NE5rVRDJ1DMjsruPIUuG1skh058.j0Z/xZLtB70E7d9FO3CwhO";
    if (this.memberUpdateFormGroup.valid) {
      this.adminService.onUpdateMember(member, this.filesToUpload)
        .subscribe(result => {
          //this.onAlertType();
          console.log(result);
          if (result.message == "User Updated Successfully") {
            this.isSuccess = true;
            this.isError = false;
            this.successMessage = "User Updated Successfully";
            this._success.next(`User Updated Successfully`);
          }
          else {
            this.isError = true;
            this.isSuccess = false;
            this.successMessage = "Failed to Update User";
            this._success.next(`Failed to Update User`);
          }
        });
    }
    else {
      this.isError = true;
      this.isSuccess = false;
      this.successMessage = "Please enter details";
    }
  }

  //createAddNewForm() {
  //  this.addNewFormGroup = this.formBuilder.group({
  //    name: [null, Validators.compose([Validators.required])]
  //    //lastName: [null, Validators.compose([Validators.required])],
  //    //email: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(30), this.validateEmail])],
  //    //dob: [null, Validators.compose([Validators.required])],
  //    //phoneNumber: [null, Validators.compose([Validators.required])],
  //    //gender: [null, Validators.compose([Validators.required])],
  //    //city: [null, Validators.compose([Validators.required])],
  //    //state: [null, Validators.compose([Validators.required])],
  //    //postcode: [null, Validators.compose([Validators.required])],
  //    //country: [null, Validators.compose([Validators.required])]
  //    //password: [null, Validators.compose([
  //    //  Validators.required,
  //    //  Validators.minLength(8),
  //    //  Validators.maxLength(35),
  //    //  this.validatePassword
  //    //])],
  //    //  confirm: [null, Validators.required]

  //    //}, { validator: this.matchingPasswords('password', 'confirm')
  //  });
  //}


  open(content) {
    this.successMessage = "";
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  onOpenContract(contract: any, id: any) {
    this.onGetAllContractsByContractId(id);
    this.modalService.open(contract).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  onReset(id: any, password: any) {

    //const formModel = this.form.value;
    //this.loading = true;
    //// In a real-world app you'd have a http request / service call here like
    //this.http.post('http://13.58.150.195:4300/users/editUser', formModel)
    //setTimeout(() => {
    //  console.log(formModel);
    //  alert('done!');
    //  this.loading = false;
    //}, 1000);

  //  alert(this.memberId);
  //  alert(id);
    password = password;
  //  alert(password);
    this.adminService.onReset(id, password).subscribe(data => {
      console.log(data);
      if (data.message == "Password updated Successfully") {
        this.isSuccessPwd = true;
        this.successMessagePwd = "Password updated Successfully";
        this._success.next(`Password updated Successfully`);
      }
      //(reason) => {
      //  this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      //};
    });
  }

  onGetContentByID() {
    //alert(this.memberId);
    //this.adminService.onGetContentByID(this.memberId).map(response => {
    //  response.json().items).subscribe(data => this.content=data,


    this.adminService.onGetContentByID(this.memberId).subscribe(result => {
      //for (let cnt of result) {        
      //}
      this.content2 = result;
      error => console.log(error);
      console.log(this.content2);
      //this.content = result.json();
      //this.content = Array.of(this.content); 
      //console.log(this.content);
      //for (let ar of this.content) {
      //  this.content.push(ar);
      //}
      //console.log(this.content);

    }, error => this.errorMessage = <any>error);
    console.log(this.errorMessage);
    return this.content2;
  }

  onGetTransactionLogByUserID() {
    this.adminService.onGetTransactionLogByUserID(this.memberId).subscribe(data => {
      this.transactionLogList = data;
      console.log(this.transactionLogList);
    });
  }

  onViewTransactionLogById(id: any) {
    this.transactionId = id;
    this.router.navigate(['/viewtransaction/', { id: this.transactionId }]);
  }

  onCreateContract(contracts) {
    console.log("contracts:" + contracts);
    contracts.memberId = this.memberId;
    contracts.createdBy = this.celebs.username;
    contracts.isActive = contracts.isActive;
    contracts._id = contracts._id;
    if (contracts._id == undefined) {
      this.adminService.onCreateContract(this.contracts).subscribe(data => {
        if (data.message == "Celebrity Contract created sucessfully") {
          this.isSuccessContract = true;
          this.successMessage = "Celebrity Contract created sucessfully";
          this._success.next(`Celebrity Contract created sucessfully`);
        }
      });
    }
    else {
      this.adminService.onUpdateContract(this.contracts).subscribe(data => {
        console.log(data);
        if (data.message == "Contract Updated Successfully") {
          this.isSuccessContract = true;
          this.successMessageContract = "Contract Updated Successfully";
          this._success.next(`Contract Updated Successfully`);
        }
      });
    }
    this.onGetAllContractsByMemberId();
  }

}
