import { Routes } from '@angular/router';

import { ViewpromotionsComponent } from '../viewpromotions/viewpromotions.component';

export const ViewPromotionsRoutes: Routes = [{
  //path: 'ViewProfile/:id',
  path:'',
  component: ViewpromotionsComponent,
  data: {
    heading: 'viewpromotions'
  }
 //{ path: '',
 //   component: ViewprofileComponent,
 //   children: [
 //     {
 //       path: 'ViewProfile/:id',
 //       component: ViewprofileComponent,
 //       canDeactivate: [CanDeactivateGuard],
 //       resolve: {
 //         crisis: CrisisDetailResolver
 //       }
 //     }
}];
