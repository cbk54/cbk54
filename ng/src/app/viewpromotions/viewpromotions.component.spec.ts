import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpromotionsComponent } from './viewpromotions.component';

describe('ViewpromotionsComponent', () => {
  let component: ViewpromotionsComponent;
  let fixture: ComponentFixture<ViewpromotionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpromotionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpromotionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
