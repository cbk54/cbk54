import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { apppromotions } from '../models/apppromotions';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-viewpromotions',
  templateUrl: './viewpromotions.component.html',
  styleUrls: ['./viewpromotions.component.scss']
})
export class ViewpromotionsComponent implements OnInit {

   promotion: apppromotions;
   promoid: any;
   isSuccess: boolean;
   isSuccessPwd: boolean;
   isError: boolean;
   successMessage: string;

   private _success = new Subject<string>();
   staticAlertClosed = false;

   constructor(private adminService: AdminService, public router: Router, public route: ActivatedRoute) {
     this.promotion = new apppromotions();
   }

   ngOnInit() {
     this.route.params.subscribe(params => {
       this.promoid = params['id'];
       console.log("promo id " +this.promoid);
     });

     setTimeout(() => this.staticAlertClosed = true, 20000);
     this._success.subscribe((message) => this.successMessage = message);
     this._success.debounceTime(5000).subscribe(() => this.successMessage = null);

    this.onviewpromotionsbyId(this.promoid);
  }

  onviewpromotionsbyId(id: any) {
    this.adminService.ongetpromotionsById(id).subscribe(result => {
      this.promotion = result;
      console.log(result);
    });
    return this.promotion;
  }

  onUpdateAppPromotion(promotion) {
   // alert(promotion._id);
    this.adminService.onpromotionappupdate(promotion).subscribe(result => {
      this.promotion = result;
      console.log(result);
      if (result.message == "appPromoMaster Updated Successfully") {
        this.isSuccess = true;
        this.successMessage = "App Promo Updated Successfully";
        this.onviewpromotionsbyId(promotion._id);
       // this._success.next(`User Updated Successfully`);
      }
      else {
        this.isError = true;
        this.successMessage = "Failed to Update App Promo";
        this.onviewpromotionsbyId(promotion._id);
        //this._success.next(`Failed to Update User`);
      }
    });
    //return this.promotion;
  }
  

}
