import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule} from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';

import { ViewserviceComponent } from '../viewservice/viewservice.component';
import { ViewserviceRoutes } from './viewservice.routing';
import { AdminService } from '../services/admin.service';


@NgModule({
  imports: [CommonModule, NgbModule, FormsModule, CustomFormsModule, ReactiveFormsModule, RouterModule.forChild(ViewserviceRoutes), NgbAccordionModule, NgbProgressbarModule, NgbTabsetModule],
  declarations: [ViewserviceComponent],
  providers: [AdminService]
})

export class ViewserviceModule {}
