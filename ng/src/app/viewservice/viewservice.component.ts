import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Service } from '../models/service';

import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-viewservice',
  templateUrl: './viewservice.component.html',
  styleUrls: ['./viewservice.component.scss']
})
export class ViewserviceComponent implements OnInit {

  service: Service;
  serviceId: any;

  constructor(public adminService: AdminService, public router: Router, public route: ActivatedRoute) {
    this.service = new Service();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.serviceId = params['id'];
      console.log(this.serviceId);
    });

    this.onGetServiceByID();
  }

  onGetServiceByID() {

    this.adminService.onGetServiceByID(this.serviceId)
      .subscribe(result => {
        this.service = result;
        console.log("activity:" + this.service);

      });
    return this.service;
  }

}
