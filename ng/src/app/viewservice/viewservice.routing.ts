import { Routes } from '@angular/router';

import { ViewserviceComponent } from '../viewservice/viewservice.component';

export const ViewserviceRoutes: Routes = [{
  //path: 'ViewProfile/:id',
  path:'',
  component: ViewserviceComponent,
  data: {
    heading: 'View Service'
  }
 //{ path: '',
 //   component: ViewprofileComponent,
 //   children: [
 //     {
 //       path: 'ViewProfile/:id',
 //       component: ViewprofileComponent,
 //       canDeactivate: [CanDeactivateGuard],
 //       resolve: {
 //         crisis: CrisisDetailResolver
 //       }
 //     }
}];
