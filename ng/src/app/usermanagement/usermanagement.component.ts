import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.scss']
})
export class UsermanagementComponent implements OnInit {

  private showActionsOne: boolean;
  private showActionsTwo: boolean;
  private showActionsThree: boolean;

  constructor() {
    this.showActionsOne = false;
    this.showActionsTwo = false;
    this.showActionsThree = false;
  }


  showPopActionsOne() {
    this.showActionsOne = true;
    this.showActionsTwo = false;
    this.showActionsThree = false;
  }
  showPopActionsTwo() {
    this.showActionsOne = false;
    this.showActionsTwo = true;
    this.showActionsThree = false;
  }
  showPopActionsThree() {
    this.showActionsOne = false;
    this.showActionsTwo = false;
    this.showActionsThree = true;
  }

  hidePopActions() {
    this.showActionsOne = false;
    this.showActionsTwo = false;
    this.showActionsThree = false;
  }

  ngOnInit() {
    //this.onGetCelebs();
  }



}
