import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SidebarModule } from 'ng-sidebar';

import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { TreeModule } from 'angular-tree-component';
import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';

import { UsermanagementComponent } from '../usermanagement/usermanagement.component';
import { UsermanagementRoutes } from './usermanagement.routing';
import { AdminService } from '../services/admin.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UsermanagementRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbProgressbarModule,
    CustomFormsModule,
    TreeModule,
    TextMaskModule,
    FileUploadModule,
    SidebarModule, NgbAccordionModule
  ],
  declarations: [UsermanagementComponent],
  providers: [AdminService]
})

export class UsermanagementModule {}
