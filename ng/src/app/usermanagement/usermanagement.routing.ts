import { Routes } from '@angular/router';

import { UsermanagementComponent } from '../usermanagement/usermanagement.component';


export const UsermanagementRoutes: Routes = [
  {
    path: '',
    component: UsermanagementComponent,
    data: {
      heading: 'User Management'
    }
  }
];
