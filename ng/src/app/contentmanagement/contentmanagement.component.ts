import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AdminService } from '../services/admin.service';

import { Contents } from '../models/members';


@Component({
  selector: 'app-contentmanagement',
  templateUrl: './contentmanagement.component.html',
  styleUrls: ['./contentmanagement.component.scss']
})
export class ContentmanagementComponent implements OnInit {

  private showActionsOne: boolean;
  private showActionsTwo: boolean;
  private showActionsThree: boolean;
  feedsList: Contents[];
  private showActions: any;
  feedId: any;

  constructor(private adminService: AdminService, public router: Router ) {
    //this.showActionsOne = false;
    //this.showActionsTwo = false;
    //this.showActionsThree = false;
    this.showActions = false;
  }


  //showPopActionsOne() {
  //  this.showActionsOne = true;
  //  this.showActionsTwo = false;
  //  this.showActionsThree = false;
  //}
  //showPopActionsTwo() {
  //  this.showActionsOne = false;
  //  this.showActionsTwo = true;
  //  this.showActionsThree = false;
  //}
  //showPopActionsThree() {
  //  this.showActionsOne = false;
  //  this.showActionsTwo = false;
  //  this.showActionsThree = true;
  //}


  showPopActions(id) {
    //  alert(id);
    this.showActions = id;
    return id;
  }

  hidePopActions() {
    this.showActions = false;
  }

  ngOnInit() {
    
    this.onGetAllFeeds();
  }
  onGetAllFeeds() {
    this.adminService.onAllFeeds().subscribe(data => {
      this.feedsList = data;
      console.log(this.feedsList);
    });
  }
  onviewfeedbyid(feedId: any) {
    this.feedId = feedId;
    //this.adminService.onGetMemberById(this.celebid);
    this.router.navigate(['/viewfeeds/', { feedId: this.feedId }]);
  }

  OnContentStatusChange(value, item) {
    
    this.adminService.onUpdateContentById(value, item).subscribe(data => {
      console.log("present status" + data);
    });
  }

}
