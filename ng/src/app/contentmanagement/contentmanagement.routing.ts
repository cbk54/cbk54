import { Routes } from '@angular/router';

import { ContentmanagementComponent } from '../contentmanagement/contentmanagement.component';


export const ContentmanagementRoutes: Routes = [
  {
    path: '',
    component: ContentmanagementComponent,
    data: {
      heading: 'Content Management'
    }
  }
];
