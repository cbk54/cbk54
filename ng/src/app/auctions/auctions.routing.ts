import { Routes } from '@angular/router';

import { AuctionsComponent } from '../auctions/auctions.component';


export const AuctionsRoutes: Routes = [
  {
    path: '',
    component: AuctionsComponent,
    data: {
      heading: 'Auctions'
    }
  }
];
