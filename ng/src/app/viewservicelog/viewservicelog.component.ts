import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AdminService } from '../services/admin.service';
import { Servicelogs } from '../models/servicelogs';

@Component({
  selector: 'app-viewservicelog',
  templateUrl: './viewservicelog.component.html',
  styleUrls: ['./viewservicelog.component.scss']
})
export class ViewservicelogComponent implements OnInit {

  servicelog : Servicelogs;
  servicelogId: any;

  constructor(public adminService: AdminService, public router: Router, public route: ActivatedRoute) {
    this.servicelog = new Servicelogs();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.servicelogId = params['id'];
      console.log(this.servicelogId);
    });

    this.onGetServiceLogByID();
  }

  onGetServiceLogByID() {

    this.adminService.onGetServiceLogByID(this.servicelogId)
      .subscribe(result => {
        this.servicelog = result;
        console.log("activity:" + this.servicelog);

      });
    return this.servicelog;
  }

}
