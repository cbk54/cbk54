import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule} from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';

import { ViewservicelogComponent } from '../viewservicelog/viewservicelog.component';
import { ViewServiceLogsRoutes } from './viewservicelog.routing';
import { AdminService } from '../services/admin.service';


@NgModule({
  imports: [CommonModule, NgbModule, FormsModule, CustomFormsModule, ReactiveFormsModule, RouterModule.forChild(ViewServiceLogsRoutes), NgbAccordionModule, NgbProgressbarModule, NgbTabsetModule],
    declarations: [ViewservicelogComponent],
  providers: [AdminService]
})

export class ViewServiceLogsModule {}
