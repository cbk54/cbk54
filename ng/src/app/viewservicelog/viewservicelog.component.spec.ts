import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewservicelogComponent } from './viewservicelog.component';

describe('ViewservicelogComponent', () => {
  let component: ViewservicelogComponent;
  let fixture: ComponentFixture<ViewservicelogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewservicelogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewservicelogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
