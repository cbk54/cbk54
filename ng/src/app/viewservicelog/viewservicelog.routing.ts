import { Routes } from '@angular/router';

import { ViewservicelogComponent } from '../viewservicelog/viewservicelog.component';

export const ViewServiceLogsRoutes: Routes = [{
  //path: 'ViewProfile/:id',
  path:'',
  component: ViewservicelogComponent,
  data: {
    heading: 'View Service Log'
  }
 //{ path: '',
 //   component: ViewprofileComponent,
 //   children: [
 //     {
 //       path: 'ViewProfile/:id',
 //       component: ViewprofileComponent,
 //       canDeactivate: [CanDeactivateGuard],
 //       resolve: {
 //         crisis: CrisisDetailResolver
 //       }
 //     }
}];
