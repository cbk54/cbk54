import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { TransactionLogs } from '../models/transactionlog';

import { AdminService } from '../services/admin.service';


@Component({
  selector: 'app-transactionlog',
  templateUrl: './transactionlog.component.html',
  styleUrls: ['./transactionlog.component.scss']
})
export class TransactionlogComponent implements OnInit {

  showActions: any;
  transactionLogList: TransactionLogs[];
  transactionId: any;

  constructor(public adminService: AdminService, public router: Router) {
  }


  showPopActions(id: any) {
    this.showActions = id;
    return id;
  }

  hidePopActions() {
    this.showActions = false;
  }

  ngOnInit() {
    this.onGetAllTransactionLogs();
  }

  onGetAllTransactionLogs() {
    this.adminService.onGetAllTransactionLogs().subscribe(data => {
      this.transactionLogList = data;
      console.log(this.transactionLogList);
    });
  }

  onViewTransactionLogById(id: any) {
    this.transactionId =id;
    this.router.navigate(['/viewtransaction/', { id: this.transactionId }]);
  }

  OnFinStatusChange(value, item) {
   // alert(value);
   // alert(item.transactionAuthCode);
    this.adminService.onupdateFinStatus(value, item).subscribe(data=> { console.log("Updated Result " +item) });
  }

}
