import { Routes } from '@angular/router';

import { TransactionlogComponent } from '../transactionlog/transactionlog.component';


export const TransactionlogRoutes: Routes = [
  {
    path: '',
    component: TransactionlogComponent,
    data: {
      heading: 'Financial Transactions'
    }
  }
];
