import { Component, Injectable  } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { CelebritiesComponent } from '../celebrities/celebrities.component';

import { Celebs } from '../models/celebs';
import { Contents } from '../models/members';
import { Content } from '../models/content';
import { Servicelogs } from '../models/servicelogs';
import { TransactionLogs } from '../models/transactionlog';
import { Orders } from '../models/orders';
import { Activities } from '../models/activities';
import { Service } from '../models/service';
import { Contracts } from '../models/contracts';
import { EventLogs } from '../models/eventlogs';


@Injectable()

export class AdminService {
  //http: Http;
    constructor(private http: Http ) {

    }

    onLogin(login) {
      return this.http.post('http://13.58.150.195:4300/logininfo/login', login)
    }

    onGetMembersList(): Observable<Celebs[]> {
      return this.http.get('http://13.58.150.195:4300/users/membersList').map((res: Response) =>
        res.json());
    }

    onGetCelebs(): Observable<Celebs[]> {
      return this.http.get('http://13.58.150.195:4300/users/getMemberByisCeleb').map((res: Response) =>
        res.json());
    }

    onGetMemberById(user_id: any) {
      //let headers = new Headers();
      //headers.append('Content-Type', 'application/json');
      //let params = new URLSearchParams();
      //params.append("user_id", user_id)

      //let params = new HttpParams().set("user_id", user_id);

      return this.http.get('http://13.58.150.195:4300/users/getMember/' + user_id + '').map((res: Response) =>
        res.json());
      // }
    }

    onRegisterMember(user) {
      return this.http.post(' http://13.58.150.195:4300/users/memberRegistrations', user).map((res: Response) =>
        res.json());
    }

    onUpdateMember(member: any, file: any)
    {
      var st: string = member.isCeleb.toString();

      //let formData: FormData = new FormData(),
      //  xhr: XMLHttpRequest = new XMLHttpRequest();
      //xhr.upload.addEventListener("progress", (ev: ProgressEvent) => {
      //  //You can handle progress events here if you want to track upload progress (I used an observable<number> to fire back updates to whomever called this upload)
      //});
      //xhr.open("POST", "/avtars", true);
      //xhr.send(formData);

      ////for (let i = 0; i < file.length; i++) {
      //formData.append("uploads[]", file[0], file[0].name);
      //console.log('form data variable :   ' + formData);
      //}
      //let headers = new Headers();
      //headers.append('Content-Type', 'application/json; charset=utf-8');
      let body: any = {};
      body.id = member._id.toString();
      body.status = member.status.toString();
      body.address= member.address;
      body.availableCredits= member.availableCredits;
      body.dateOfBirth = member.dateOfBirth;
      body.email = member.email;
      body.gender = member.gender;
      body.location = member.location;
      body.loginType = member.loginType;
      body.mobileNumber = member.mobileNumber;
      body.name = member.name;
      body.password = member.password;
      body.username = member.username;
      body.preferences = member.preferences;
      body.IsDeleted = member.IsDeleted.toString();
      body.aboutMe = member.aboutMe;
      body.avtar_imgPath = member.avtar_imgPath;
      body.avtar_originalname = member.avtar_originalname;
      body.created_at = member.created_at;
      body.isCeleb =st;
      body.lastName = member.lastName;
      body.prefix = member.prefix;
      body.role = member.role;
      body.isTrending = member.isTrending.toString();
      body.isEditorChoice = member.isEditorChoice.toString();
      body.isOnline = member.isOnline.toString();
      body.isPromoted = member.isPromoted.toString();
     // body.status = member.status.toString();
      //body.updated_at = member.updated_at;
      body.updated_at = member.updated_at == null ? "" : member.updated_at;
      body.v = member.__v.toString();
      body.celebToManager = member.celebToManager;
      console.log(body);
      return this.http.post('http://13.58.150.195:4300/users/editUser',body).map((res: Response) =>
        res.json());
    }
	
    onUploadProfilePic(id:any,profilePic: any) {
      console.log(profilePic);
      //let headers = new Headers();
      //headers.append('Content-Type', 'multipart/form-data');
      let body: any = {};
      body.id = id.toString();
      body.avtar_imgPath = profilePic;
      return this.http.post('http://13.58.150.195:4300/users/editUser',body).map((res: Response) =>
        res.json());
    }

    onAllFeeds(): Observable<Content[]> { 
      return this.http.get('http://13.58.150.195:4300/feeddata/allFeed').map((res: Response) =>
        res.json());
    }

    onStatuUpdate(id: any, status: any) {
      let body: any = {};
      body.id = id;
      body.status = status.toString();
      console.log(body);
      return this.http.post('http://13.58.150.195:4300/admin/editStatus', body).map((res: Response) => res.json());
    }

    onReset(email: any, password: any) {
      let body: any = {};
      body.email = email;
      body.password = password;
      return this.http.post('http://13.58.150.195:4300/admin/resetPasswordByEmail', body).map((res: Response) => res.json());
    }



    onGetContentByID(id: any): Observable<any[]> {
      // alert(id);
      // return this.http.get('http://192.168.2.137:4300/feeddata/getFeedByMemberId/' + id + '').map((res: Response) => res.json());    
      return this.http.get('http://13.58.150.195:4300/feeddata/getFeedByMemberId/' + id + '').map((res: Response) => res.json());
    }

    onGetFeedById(feedId: any) {
      //let headers = new Headers();
      //headers.append('Content-Type', 'application/json');
      //let params = new URLSearchParams();
      //params.append("user_id", user_id)

      //let params = new HttpParams().set("user_id", user_id);

      return this.http.get('http://13.58.150.195:4300/feeddata/getFeedByFeedID/' + feedId + '').map((res: Response) =>
        res.json());
      // }
    }

    onfileupload(id: any, formdata: any) {
      //formdata.id = "5a9cd36741249d75a4d62834";
      //formdata.avtar_imgPath = "2";
      return this.http.post('http://13.58.150.195:4300/users/editUser', formdata).map((res: Response) =>
        res.json());
    }

    onGetAllActivities(): Observable<Activities[]> {
      return this.http.get('http://13.58.150.195:4300/activityTransaction/getAll').map((res: Response) =>
        res.json());
    }

    onGetActivitiesByID(id: any) {
      return this.http.get('http://13.58.150.195:4300/activityTransaction/findByActivityTransactionId/' + id + '').map((res: Response) => res.json());
    }

    onGetAllServiceLogs(): Observable<Servicelogs[]> {
      return this.http.get('http://13.58.150.195:4300/serviceTransaction/getAll').map((res: Response) =>
        res.json());
    }

    onGetServiceLogByID(id: any) {
      return this.http.get('http://13.58.150.195:4300/serviceTransaction/findByServiceTransactionId/' + id + '').map((res: Response) => res.json());
    }

    onGetAllServices(): Observable<Service[]> {
      return this.http.get('http://13.58.150.195:4300/serviceschedule/getAll').map((res: Response) =>
        res.json());
    }

    onGetServiceByID(id: any) {
      return this.http.get('http://13.58.150.195:4300/serviceschedule/getServiceScheduleInfo/' + id + '').map((res: Response) => res.json());
    }

    onGetAllTransactionLogs(): Observable<TransactionLogs[]> {
      return this.http.get('http://13.58.150.195:4300/financialTransaction/getAll').map((res: Response) =>
        res.json());
    }

    onGetTransactionLogByID(id: any) {
      return this.http.get('http://13.58.150.195:4300/financialTransaction/findByfinancialTransactionId/' + id + '').map((res: Response) => res.json());
    }

    onGetTransactionLogByUserID(id: any) {
      return this.http.get('http://13.58.150.195:4300/financialTransaction/getByUserID/' + id + '').map((res: Response) => res.json());
    }

    onGetAllOrders(): Observable<Orders[]> {
      return this.http.get('http://13.58.150.195:4300/orders/getAll').map((res: Response) =>
        res.json());
    }

    onGetOrdersByID(id: any) {
      return this.http.get('http://13.58.150.195:4300/orders/findOrderId/' + id + '').map((res: Response) => res.json());
    }

    onGetAllEventLogs(): Observable<EventLogs[]> {
      return this.http.get('http://13.58.150.195:4300/auditlog/getAll').map((res: Response) =>
        res.json());
    }

    onGetEventLogsByID(id: any) {
      return this.http.get('http://13.58.150.195:4300/auditlog/getauditlog/' + id + '').map((res: Response) => res.json());
    }
    
    onGetAllContractsByMemberId(id:any): Observable<Contracts[]> {
      return this.http.get('http://13.58.150.195:4300/celebritycontract/getContractsByUserID/' + id + '').map((res: Response) =>
        res.json());
    }

    onGetAllContractsByContractId(id: any): Observable<Contracts> {
      return this.http.get('http://13.58.150.195:4300/celebritycontract/getContractByID/' + id + '').map((res: Response) =>
        res.json());
    }

    onCreateContract(contract) {
      let body: any = {};
      body.memberId = contract.memberId.toString();
      body.serviceType = contract.serviceType;
      body.startDate = contract.startDate.toString();
      body.endDate = contract.endDate.toString();
      body.dateOfBirth = contract.dateOfBirth;
      body.serviceCredits = contract.serviceCredits.toString();
      body.isActive = contract.isActive == undefined ? false.toString() : contract.isActive.toString();
      body.remarks = contract.remarks;
      body.createdBy = contract.createdBy;
      return this.http.post('http://13.58.150.195:4300/celebritycontract/create', body).map((res: Response) =>
        res.json());
    }

    onUpdateContract(contract) {
      return this.http.put('http://13.58.150.195:4300/celebritycontract/edit/' + contract._id + '', contract).map((res: Response) =>
        res.json());
    }


    onupdateFinStatus(value,fin) {
      let finbody: any = {};
      finbody.memberId = fin.memberId;
      finbody.financialType;
      finbody.value;
      finbody.transactionMode;
      finbody.transactionValue;
      finbody.transactionAuthCode;
     // http://13.58.150.195:4300/financialTransaction/financialTransaction/5aaafe10f6bffb0a5f8d3452
      return this.http.put('http://13.58.150.195:4300/financialTransaction/financialTransaction/'+ fin._id+'',fin).map((res: Response) =>
        res.json());
    }

    onUpdateOrderStatus(value, order) {
      return this.http.put('http://13.58.150.195:4300/orders/editOrders/' + order._id + '', order).map((res: Response) => res.json());
    }

    onGetallAppPromotions(): Observable<any[]> {
      return this.http.get('http://13.58.150.195:4300/appPromoMaster/getAll').map((res: Response) => res.json());
    }

    ongetpromotionsById(id: any): Observable<any> {
      console.log("service ID " + id);
      return this.http.get('http://13.58.150.195:4300/appPromoMaster/getAppPromoRequestInfoById/' + id + '').map((res: Response) => res.json());
    }

    onpromotionappupdate(promotion) {
      return this.http.put('http://13.58.150.195:4300/appPromoMaster/setAppPromoStatus/' + promotion._id + '', promotion).map((res: Response) =>
        res.json());
    }

    onGetAllCelebRequests() {
      return this.http.get('http://13.58.150.195:4300/celebrequest/getAll').map((res: Response) => res.json());
    }

    onGetCelebRequestById(member_id: any) {
      return this.http.get('http://13.58.150.195:4300/users/getMember/' + member_id + '').map((res: Response) =>
        res.json());
    }


    onCreateCelebrityContract(contract) {
      return this.http.post('http://13.58.150.195:4300/celebritycontract/create', contract).map((res: Response) =>
        res.json());
    }

    onUpdateCelebrityContract(contract) {
      return this.http.put('http://13.58.150.195:4300/celebrequest/edit/' + contract._id + '', contract).map((res: Response) =>
        res.json());
    }

    onUpdateContentById(value, content) {
      let Contentbody: any = {};
      Contentbody._id = content._id
      Contentbody.status = value;
      //Contentbody.financialType;
      //Contentbody.value;
      //Contentbody.transactionMode;
      //Contentbody.transactionValue;
      //Contentbody.transactionAuthCode;
      return this.http.put('http://13.58.150.195:4300/feeddata/edit/' + content._id + '', Contentbody).map((res: Response) => res.json());
    }

    onUpdateContentSettings(settings) {
      let contentBodysettings: any = {
        downloadOptions: {}};
      //let downloadOptions :any= {}
      contentBodysettings.profanityCheck = settings.profanityCheck;
      contentBodysettings.offlineStatus = settings.offlineStatus;
      contentBodysettings.isPrometed = settings.isPrometed; 
      contentBodysettings.downloadOptions.downloadStatus = settings.downloadOptions.downloadStatus;
      contentBodysettings.viewOptions = settings.viewOptions;
      return this.http.put('http://13.58.150.195:4300/feeddata/edit/' + settings._id + '', contentBodysettings).map((res: Response) => res.json());
    }

    onUpdateBootSettings(settings) {
      //let settings2: any = {
      //  boostSettings: { } };
      //alert(settings._id);

      //alert(settings.boostSettings.proposedBudget);
      //alert(settings.boostSettings.proposedReach);

      //boostSettings.proposedBudget = settings.boostSettings.proposedBudget;
      //boostSettings.proposedReach = settings.boostSettings.proposedReach;
      return this.http.put('http://13.58.150.195:4300/feeddata/edit/' + settings._id + '', settings).map((res: Response) => res.json());

    }


}
