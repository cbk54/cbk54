import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { Activities } from '../models/activities';

import { AdminService } from '../services/admin.service';


@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss']
})
export class SchedulerComponent implements OnInit {

  showActions: any;
  activitiesList: Activities[];
  activityId: any;

  constructor(public adminService: AdminService, public router: Router) {
  }


  showPopActions(id: any) {
    this.showActions = id;
    return id;
  }

  hidePopActions() {
    this.showActions = false;
  }

  ngOnInit() {
    this.onGetAllActivities();
  }

  onGetAllActivities() {
    this.adminService.onGetAllActivities().subscribe(data => {
      this.activitiesList = data;
      console.log(this.activitiesList);
    });
  }

  onViewActivityById(id: any) {
    this.activityId = id;
    this.router.navigate(['/viewschedular/', { id: this.activityId }]);
  }



}
