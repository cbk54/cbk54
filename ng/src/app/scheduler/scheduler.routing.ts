import { Routes } from '@angular/router';

import { SchedulerComponent } from '../scheduler/scheduler.component';


export const SchedulerRoutes: Routes = [
  {
    path: '',
    component: SchedulerComponent,
    data: {
      heading: 'Activities'
    }
  }
];
