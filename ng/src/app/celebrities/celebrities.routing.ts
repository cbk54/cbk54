import { Routes } from '@angular/router';

import { CelebritiesComponent } from '../celebrities/celebrities.component';


export const CelebritiesRoutes: Routes = [
  {
    path: '',
    component: CelebritiesComponent,
    data: {
      heading: 'Celebrities'
    }
  }
];
