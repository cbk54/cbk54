import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AdminService } from '../services/admin.service';
import { Celebs } from '../models/celebs';


@Component({
  selector: 'app-celebrities',
  templateUrl: './celebrities.component.html',
  styleUrls: ['./celebrities.component.scss']
})

export class CelebritiesComponent implements OnInit {
  celebs: Array<any>;
  listofcelebs: Celebs[];
  celebid: any;
  date: any;
  //adminService: AdminService;
  private showActions: any;
  


  constructor(private adminService: AdminService, public router: Router ) {
    this.showActions = false;
    this.date = new Date(); 
    
   }
  

  showPopActions(id) {
  //  alert(id);
    this.showActions = id;    
    return id;
  }




  
  hidePopActions() {
    this.showActions = false;
  }
    
  ngOnInit() {
    this.onGetCelebs();
  }


  onGetCelebs() {
    this.adminService.onGetCelebs().subscribe(data => {
      this.listofcelebs = data;
      //console.log(this.listofcelebs);
    });
  }

  onviewcelebbyid(memId: any) {
    this.celebid = memId;
    //this.adminService.onGetMemberById(this.celebid);
    this.router.navigate(['/ViewProfile/', { memId: this.celebid }]);
  }

  onresetpassword(id: any) {
    //alert('reset password for id ' + id + '');
    //alert(celebs.password);
      this.router.navigate(['/resetpassword', { id: id }]);
      this.celebid = id;
      return id;
  }

  onchangestatus(id, status) {
    //alert(id);
    //alert(status);
    //if (status == true) {
    //  status = false;
    //  alert(status);
    //}
    //else {
    //  status = true;
    //}
    this.adminService.onStatuUpdate(id,status).subscribe(data => {
      console.log(data);
      //this.onGetCelebs();
    });
  }


  ondatediff(date) {
   
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    //if (dd < 10) {
    //  dd = '0' + dd
    //}
    //if (mm < 10) {
    //  mm = '0' + mm
    //}
    //today = yyyy + '/' + mm + '/' + dd;
    //$scope.today = today;
    var date2 = new Date(today);
    var date1 = new Date(date);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var res = Math.ceil(timeDiff / (1000 * 3600 * 24));
    //return $scope.dayDifference;
    //console.log(res);
    return res;
    //console.log(date1);
    //console.log(date2);
    //var dd: Date = date2;

    //var diffc = date1.getTime() - date2.getTime();
    ////var date3:  Date = date2;
    ////getTime() function used to convert a date into milliseconds. This is needed in order to perform calculations.

    // var days = Math.round(Math.abs(diffc / (1000 * 60 * 60 * 24)));
    // //this is the actual equation that calculates the number of days.
    //// var timeDiff = Math.abs(date3.getTime() - date1.getTime());
    // //return timeDiff;
    // return days;
  }



  
}
