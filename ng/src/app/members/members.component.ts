import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { AdminService } from '../services/admin.service';
import { Celebs } from '../models/celebs';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {

  //@Input() count: number;
  //@Output() showPopActions = new EventEmitter<any>();
  showActions: any;
  memberId: any;
  members: Celebs[];

  celebrequests: Celebs[];
  celebreqcount: any;

  constructor(private adminServices: AdminService, public router: Router) {
  }

  showPopActions(id: any) {
    this.showActions = id;
    return id;
  }

  //addNewEntry(): void {
  //  this.count++;
  //  this.showPopActions.emit({
  //    click: this.count,
  //    showActionsOne: this.showActionsOne=true
  //  })
  //  //this.showActionsOne = true;
  //   }

  hidePopActions() {
    this.showActions = false;
  }

  ngOnInit() {
    this.onGetMembersList();
    this.onGetCelebRequestsCount();
    //this.onGetCelebs();
  }

  onGetMembersList() {

    this.adminServices.onGetMembersList().subscribe(data => {
      this.members = data;
      this.members = this.members.filter(m => m.isCeleb == false);
      //console.log(this.members);
      //this.membersCount=Object.keys(this.members).length;
      //console.log(this.membersCount);
    });
  }

  onViewProfile(memId: any) {
    this.memberId = memId;
   // this.router.navigate(['/', 'viewprofile']);
    this.router.navigate(['/ViewProfile/', { memId:this.memberId }]);
  }

  onchangestatus(id, status) {
   
    this.adminServices.onStatuUpdate(id, status).subscribe(data => {
      console.log(data);
      
    });


  }

  ondatediff(date) {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    //if (dd < 10) {
    //  dd = '0' + dd
    //}
    //if (mm < 10) {
    //  mm = '0' + mm
    //}
    //today = yyyy + '/' + mm + '/' + dd;
    //$scope.today = today;
    var date2 = new Date(today);
    var date1 = new Date(date);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var res = Math.ceil(timeDiff / (1000 * 3600 * 24));
    //return $scope.dayDifference;
    //console.log(res);
    return res;
    //console.log(date1);
    //console.log(date2);
    //var dd: Date = date2;

    //var diffc = date1.getTime() - date2.getTime();
    ////var date3:  Date = date2;
    ////getTime() function used to convert a date into milliseconds. This is needed in order to perform calculations.

    // var days = Math.round(Math.abs(diffc / (1000 * 60 * 60 * 24)));
    // //this is the actual equation that calculates the number of days.
    //// var timeDiff = Math.abs(date3.getTime() - date1.getTime());
    // //return timeDiff;
    // return days;
  }

  onGetCelebRequestsCount() {
    this.adminServices.onGetAllCelebRequests().subscribe(data => {
      this.celebrequests = data
      this.celebreqcount = this.celebrequests.length;
    });
    return this.celebreqcount;
  }

  GetAllCelebreqsLink() {
    this.router.navigate(['/celebrequests/']);
  }
}
