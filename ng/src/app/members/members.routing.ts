import { Routes } from '@angular/router';

import { MembersComponent } from '../members/members.component';


export const MembersRoutes: Routes = [
  {
    path: '',
    component: MembersComponent,
    data: {
      heading: 'Members'
    }
  }
];
