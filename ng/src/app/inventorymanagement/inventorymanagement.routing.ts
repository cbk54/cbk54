import { Routes } from '@angular/router';

import { InventorymanagementComponent } from '../inventorymanagement/inventorymanagement.component';


export const InventorymanagementRoutes: Routes = [
  {
    path: '',
    component: InventorymanagementComponent,
    data: {
      heading: 'Inventory Management'
    }
  }
];
