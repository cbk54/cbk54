import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inventorymanagement',
  templateUrl: './inventorymanagement.component.html',
  styleUrls: ['./inventorymanagement.component.scss']
})
export class InventorymanagementComponent implements OnInit {

  private showActionsOne: boolean;
  private showActionsTwo: boolean;
  private showActionsThree: boolean;

  constructor() {
    this.showActionsOne = false;
    this.showActionsTwo = false;
    this.showActionsThree = false;
  }


  showPopActionsOne() {
    this.showActionsOne = true;
    this.showActionsTwo = false;
    this.showActionsThree = false;
  }
  showPopActionsTwo() {
    this.showActionsOne = false;
    this.showActionsTwo = true;
    this.showActionsThree = false;
  }
  showPopActionsThree() {
    this.showActionsOne = false;
    this.showActionsTwo = false;
    this.showActionsThree = true;
  }

  hidePopActions() {
    this.showActionsOne = false;
    this.showActionsTwo = false;
    this.showActionsThree = false;
  }

  ngOnInit() {
    //this.onGetCelebs();
  }



}
